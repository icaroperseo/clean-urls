[//]: # (Filename: README.md)
[//]: # (Author: Icaro Perseo)
[//]: # (Contact: <icaroperseo[at]protonmail[dot]com>)
[//]: # (Created: 29 oct 2016 23:12:52)
[//]: # (Last Modified: 01 nov 2016 02:13:44)

# Clean URLs (Mozilla Firefox Web Extension)

This project is a fork of [Pure
URL](https://addons.mozilla.org/en-US/firefox/addon/pure-url/) add-on, ported
as Web Extension from [Google
Chrome](https://chrome.google.com/webstore/detail/pure-url/mphlceppeighhlbggdkmcoieeckjnfjj).

Special thanks to the original author, [Evgeny Vrublevsky](http://veg.by/en/)
(_VEG_), for her work and dedication in creating this add-on.

This project is covered by the [GNU General Public License, version
3.0](http://www.gnu.org/licenses/gpl-3.0.html) license.

## Roadmap

* [x] Porting the original complement as Firefox Web Extension.
* [ ] Web extension reimplementation and improved functionality.
* [ ] Porting the web extension to Google Chrome/Chromium.
