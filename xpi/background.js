var rule_global = new RegExp('(^|&(amp;)?)(utm_source|utm_medium|utm_term|utm_content|utm_campaign|utm_reader|utm_place|ga_source|ga_medium|ga_term|ga_content|ga_campaign|ga_place|yclid|_openstat|fb_action_ids|fb_action_types|fb_ref|fb_source|action_object_map|action_type_map|action_ref_map)=[^&]*', 'ig');
var rule_youtube = new RegExp('(^|&(amp;)?)(feature)=[^&]*', 'ig');
var rule_facebook = new RegExp('(^|&(amp;)?)(ref|fref|hc_location)=[^&]*', 'ig');
var rule_imdb = new RegExp('(^|&(amp;)?)(ref_)=[^&]*', 'ig');

chrome.webRequest.onBeforeRequest.addListener(function(info)
{
    var path = info.url;
    var host = info.url.match(/https?:\/\/(?:www\.)?([-_.\w\d]+)/i)[1].toLowerCase();
    var qpos = path.indexOf('?');
    if (rule_global !== null && qpos > -1)
    {
        var args = path.substring(qpos + 1, path.length);

        // Hardcode, sorry, I'll fix it later
        var cleared = args.replace(rule_global, '').replace(/^(&(amp;)?)+/i, '');
        if (host === 'youtube.com')  cleared = cleared.replace(rule_youtube, '').replace(/^(&(amp;)?)+/i, '');
        if (host === 'facebook.com') cleared = cleared.replace(rule_facebook, '').replace(/^(&(amp;)?)+/i, '');
        if (host === 'imdb.com')     cleared = cleared.replace(rule_imdb, '').replace(/^(&(amp;)?)+/i, '');

        if (args !== cleared)
        {
            path = path.substring(0, qpos);
            if (cleared) path += '?' + cleared;
            return {redirectUrl: path};
        }
    }
},
{urls: ['https://*/*?*', 'http://*/*?*'], types: ['main_frame']}, ['blocking']);
