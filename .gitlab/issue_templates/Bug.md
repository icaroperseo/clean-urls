[//]: # (This template is for creating bug reports. If you're NOT describing a bug, please use another template for this purpose. Thank you!)

## Expected Behavior

[//]: # (Please, tell us in detail what should happen:)

## Current Behavior

[//]: # (Please, tell us in detail what happens instead of the expected behavior:)

## Context

[//]: # (How has this issue affected you? What are you trying to accomplish?)
[//]: # (Providing context helps us come up with a solution that is most useful in the real world:)

## My Environment

* Web browser and version used: 
* Web extension version used: 
* Operating System, version and architecture (32 or 64 bits): 
